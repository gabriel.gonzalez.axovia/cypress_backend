const { defineConfig } = require("cypress");
const { MongoClient } = require("mongodb");
const mysql = require("mysql");

function queryCon(query, config) {
  // creates a new mysql connection using credentials from cypress.json env's
  const connection = mysql.createConnection(config.env.db);
  // start connection to db
  connection.connect();
  // exec query + disconnect to db as a Promise
  return new Promise((resolve, reject) => {
    connection.query(query, (error, results) => {
      if (error) reject(error);
      else {
        connection.end();
        return resolve(results);
      }
    });
  });
}

async function connect(client){
  await client.connect();
  return client.db("sample_mflix");
}

module.exports = defineConfig({
  e2e: {
    async setupNodeEvents(on, config) {
      const client = new MongoClient(config.env.mongo)
      on("task",{
        queryDB:(query)=>{
          return queryCon(query,config)
        },
        async getListing(){
          try{
            const db= await connect(client)
            const listingAndRev= db.collection("comments");
            return await listingAndRev.find({}).limit(50).toArray()
          }catch(ex){
            console.error(ex)
          }
          finally{
            await client.close()
          }
        },
        async createcomment(coment){
          try{
            const db= await connect(client)
            const listingAndRev= db.collection("comments");
            return await listingAndRev.insertOne(coment);
          }catch(ex){
            console.error(ex)
          }
          finally{
            await client.close()
          }
        },
        async clearComment(){
          try {
            const db = await connect(client);
            const listingsAndReviews = db.collection("comments");
            return await listingsAndReviews.remove({});
          } catch (error) {
            console.error(error);
          } finally {
            await client.close();
          }
        }
      })
    },
    baseUrl: "http://localhost:3000",
  },
});
