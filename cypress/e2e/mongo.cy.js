describe("Probando mongo", function () {
  it("Select de mongo", function () {
    cy.task("getListing").then((result) => {
      console.log(result);
      expect(result).to.have.length(50);
    });
  });

  it("create de mongo", function () {
    cy.task("createcomment", {
      name: "Javier Hernandez",
      email: "javier_hernandez@fakegmail.com",
      movie_id: "573a1390f29313caabcd4323",
      text: "Eius veritatis vero facilis quaerat fuga temporibus. Praesentium exped...",
      date: "2022-07-07T04:56:07.000+00:00",
    }).then((result) => {
      console.log(result);
      expect(result.acknowledged).to.eq(true);
      expect(result).to.haveOwnPropertyDescriptor("insertedId");
    });
  });

  after(() => {
    cy.task("clearComment");
  });
});
