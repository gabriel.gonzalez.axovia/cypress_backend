describe("Probando los request", function () {
  it("Debe crear un empleado", function () {
    cy.request({
      url: "employees",
      method: "POST",
      body: {
        first_name: "Francisco",
        last_name: "Echeverria",
        email: "javier@platzi.com",
      },
    }).then(function (response) {
      expect(response.status).to.eq(201);
      expect(response.body).to.have.property("id");

      const id = response.body.id;

      cy.wrap(id).as("id");
    });
  });

  it("Debemos de validar que se haya creado en la base de datos", () => {
    cy.request("GET", "employees").then((response) => {
      expect(response.body[response.body.length - 1].first_name).to.eq(
        "Francisco"
      );
    });
  });

  it("Debe cambiarle el correo", function () {
    cy.request({
      url: "employees/" + this.id,
      method: "PUT",
      body: {
        first_name: "Francisco",
        last_name: "Echeverria",
        email: "francisco@platzi.com",
      },
    }).then(function (response) {
      expect(response.status).to.eq(200);
      expect(response.body).to.have.property("id");
    });
  });

  it("Debe eliminar el empleado", function () {
    cy.request({
      url: "/employees/" + this.id,
      method: "DELETE",
    }).then((response) => {
      expect(response.status).to.eq(200);
    });
  });
});
