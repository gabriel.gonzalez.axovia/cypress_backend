describe("Probando los headers", () => {
  it("validar el header y el type", () => {
    cy.request("/employees")
      .its("headers")
      .its("content-type")
      .should("include", "application/json");
  });
});
