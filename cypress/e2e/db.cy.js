describe("Pruebas de base de datos", function () {
  after(function () {
    cy.task("queryDB", "Delete from pruebas").then((results) => {
      cy.log(results);
    });
  });
  it("Select a query", function () {
    cy.task("queryDB", "select * from pruebas").then((results) => {
      cy.log(results);
    });
  });

  it("insert", function () {
    cy.task(
      "queryDB",
      "insert into pruebas (nombre,apellido) values('Javier','Lopez');"
    ).then((results) => {
      cy.log(results);
      expect(results.affectedRows).to.equal(1);
      cy.wrap(results.insertId).as("id");
    });
  });

  it("Select para comprobar que este la prueba pasada", function () {
    cy.task("queryDB", "Select * from pruebas where id=" + this.id).then(
      (results) => {
        cy.log(results);
        expect(results[0].nombre).to.eq("Javier");
        expect(results[0].apellido).to.eq("Lopez");
      }
    );
  });

  it("update para comprobar que este la prueba pasada", function () {
    cy.task("queryDB", "update pruebas set apellido='Lopez Hernandez' where id=" + this.id).then(
      (results) => {
        cy.log(results);
        expect(results.affectedRows).to.equal(1);
      }
    );
  });

  it("Delete para comprobar que este la prueba pasada", function () {
    cy.task("queryDB", "Delete from pruebas where id=" + this.id).then(
      (results) => {
        cy.log(results);
        expect(results.affectedRows).to.equal(1);
        expect(results.serverStatus).to.equal(2);
      }
    );
  });

  
});
